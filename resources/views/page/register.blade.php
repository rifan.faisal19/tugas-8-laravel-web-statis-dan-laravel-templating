@extends('page.master')
   
   @section('title')
   Buat Account Baru
   @endsection

   @section ('content')
   <form action="/send" method="post">
    @csrf
    <h2>Sign Up Form</h2><br>
    <label>First Name :</label><br>
    <input type="text" name="nama_depan"><br><br>
    <label>Last Name : </label><br>
    <input type="text" name="nama_belakang"><br><br>
    <label>Gender :</label><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br><br>
    <label>Nationality</label><br>
    <select name="kebangsaan" id="kebangsaan">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Germany">Germany</option>
        <option value="Turkey">Turki</option>    
    </select><br><br>
    <label>Language Spoken</label><br>
    <input type="checkbox" id="bahasa1" name="bahasa1" value="Indonesia">Indonesia<br>
    <input type="checkbox" id="bahasa2" name="bahasa2" value="English">English<br>
    <input type="checkbox" id="bahasa3" name="bahasa3" value="Other">Other<br><br>
    <label>Bio</label><br>
    <textarea name="bio" rows="10" cols="30"></textarea><br><br>
    <input type="submit" value="send">
    </form>
   @endsection



